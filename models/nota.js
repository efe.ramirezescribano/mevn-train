import mongoose from 'mongoose'
const Schema = mongoose.Schema

const notaSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Nombre obligatorio']
    },
    description: {type: String},
    alias: {type: String },
    date: { type: Date, default: Date.now },
    activo: {type: Boolean, default: true}
})

//create mongoose model
const Nota = mongoose.model('Nota', notaSchema)

export default Nota
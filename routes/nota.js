import express from 'express'
const router = express.Router()

import Nota from '../models/nota'
 
// Add note
router.post('/add-note', async(req, res) => {
    const body = req.body
    try {
        const notaToDB = await Nota.create(body)
        res.status(200).json(notaToDB)
    } catch (error) {
        return res.status(500).json({
            mensaje: 'Ocurrio un error guardando el elemento.',
            error
        }) //server error
    }
})

//get with parameters
router.get('/note/:id', async(req, res) => {
    //params se pasa por la url
    const _id = req.params.id
    try {
        const notaDB = await Nota.findOne({_id})
        res.json(notaDB)
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error buscando el elemento.',
            error
        })
    }
})

// get all notes
router.get('/notes', async(req, res) => {
    try {
        const notaDB = await Nota.find()
        res.json(notaDB)
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error obteniendo todos los documentos.',
            error
        })
    }
})

//delete note
router.delete('/note/:id', async(req, res) => {
    try {
        const _id = req.params.id
        const notaDB = await Nota.findByIdAndDelete({_id})
        !notaDB ? res.status(404).json({mensaje: 'Id not found'}) : res.json(notaDB)
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error eliminando el elemento seleccionado.',
            error
        })
    }
})

//update note
router.put('/note/:id', async(req, res) => {
    const _id = req.params.id
    const body = req.body

    try {
        const notaDB = await Nota.findByIdAndUpdate(
            _id,
            body,
            {new: true}
        )
        res.json(notaDB);
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error eliminando el elemento seleccionado.',
            error
        })
    }

})
//Export routes
module.exports = router;
import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
// import axios from 'axios'

Vue.use(Vuex)
export function defaultState () {
  return {
    allNotes: []
  }
}
const state = defaultState()
export default new Vuex.Store({
  state,
  actions,
  getters,
  mutations,
  modules: {
  }
})

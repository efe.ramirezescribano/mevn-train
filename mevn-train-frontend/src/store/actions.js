import axios from 'axios'

export const getAllNotes = ({ commit }) => {
  return axios.get('/notes')
  .then(res => {
    commit('setAllNotes', res.data)
    console.log(res.data)
  })
  .catch(e => {
    console.log('error getting notes: ', e.response)
  })
}

export const addNote = async ({ dispatch }, newNote) => {
  try {
    axios.post('/add-note', newNote )
      .then(res => {
        console.log('RES', res)
        dispatch('getAllNotes')
      })
      .catch(e => {
        console.log(e.response)
      })
  }
  catch (error) {
    console.log('error async', error)
  }
}
/* 
CON babel ahora podemos usar los métodos de import para las dependencias
const express = require('express')
const app = express()
// para acceder al directorio actual (donde esté desplegada/alojada la app)
const path = require('path')
const morgan = require('morgan')
const cors = require('cors') */

import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import path from 'path'
import mongoose from 'mongoose'

const app = express()

//mongodb config
const uri = 'mongodb://localhost:27017/mevn-train-db'
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true
}
//mongo connection
mongoose.connect(uri, options).then(
    () => {
        console.log('MONGODB CONNECTION SUCCESS')
    },
    err => { console.log('MONGODB NOT CONNECTED ASDASDASDASD >:') }
)

//MIDDLEWARES usage
app.use(morgan('tiny'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

//routes
/* app.get('/', (req, res) => {
    res.send(`working on ${app.get('port')}`)
}) */
app.use('/api', require('./routes/nota'))

//Middleware para Vue.js router modo history -must be above routes-
const history = require('connect-history-api-fallback')
app.use(history())
app.use(express.static(path.join(__dirname, 'public')))


//global port config var
app.set('port', process.env.PORT || 3000)
app.listen(app.get('port'), () => {
    console.log(`working on ${app.get('port')}`)
})

